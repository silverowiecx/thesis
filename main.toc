\contentsline {paragraph}{}{1}{paragraph*.1}%
\contentsline {paragraph}{}{1}{paragraph*.2}%
\contentsline {paragraph}{}{2}{paragraph*.3}%
\contentsline {paragraph}{}{2}{paragraph*.4}%
\contentsline {chapter}{\numberline {1}Introduction}{4}{chapter.1}%
\contentsline {section}{\numberline {1.1}What is Blockchain and Cryptocurrency?}{4}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Blockchain as a database}{4}{subsection.1.1.1}%
\contentsline {paragraph}{}{4}{paragraph*.6}%
\contentsline {subsection}{\numberline {1.1.2}The beginnings}{5}{subsection.1.1.2}%
\contentsline {paragraph}{}{5}{paragraph*.7}%
\contentsline {subsection}{\numberline {1.1.3}Cryptocurrency in example of Bitcoin}{5}{subsection.1.1.3}%
\contentsline {paragraph}{}{5}{paragraph*.8}%
\contentsline {paragraph}{}{5}{paragraph*.9}%
\contentsline {paragraph}{}{6}{paragraph*.10}%
\contentsline {subsection}{\numberline {1.1.4}Blockchain as a decentralized network}{6}{subsection.1.1.4}%
\contentsline {paragraph}{}{6}{paragraph*.11}%
\contentsline {paragraph}{}{6}{paragraph*.12}%
\contentsline {paragraph}{}{7}{paragraph*.13}%
\contentsline {section}{\numberline {1.2}What is a blockchain transaction? Public and private key}{8}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Blockchain transaction}{8}{subsection.1.2.1}%
\contentsline {paragraph}{}{8}{paragraph*.14}%
\contentsline {paragraph}{}{8}{paragraph*.15}%
\contentsline {subsection}{\numberline {1.2.2}Public and private key}{9}{subsection.1.2.2}%
\contentsline {paragraph}{}{9}{paragraph*.16}%
\contentsline {paragraph}{}{9}{paragraph*.17}%
\contentsline {paragraph}{}{10}{paragraph*.18}%
\contentsline {paragraph}{}{10}{paragraph*.19}%
\contentsline {section}{\numberline {1.3}Cryptocurrency exchanges and blockchain transaction processing}{10}{section.1.3}%
\contentsline {paragraph}{}{10}{paragraph*.20}%
\contentsline {section}{\numberline {1.4}Why a safe system is required for private key storage}{11}{section.1.4}%
\contentsline {paragraph}{}{11}{paragraph*.21}%
\contentsline {paragraph}{}{11}{paragraph*.22}%
\contentsline {paragraph}{}{11}{paragraph*.23}%
\contentsline {section}{\numberline {1.5}Other cryptocurrencies and blockchain systems}{12}{section.1.5}%
\contentsline {paragraph}{}{12}{paragraph*.24}%
\contentsline {subsection}{\numberline {1.5.1}Ethereum}{12}{subsection.1.5.1}%
\contentsline {paragraph}{}{12}{paragraph*.25}%
\contentsline {paragraph}{}{12}{paragraph*.26}%
\contentsline {subsection}{\numberline {1.5.2}Golem Network}{13}{subsection.1.5.2}%
\contentsline {paragraph}{}{13}{paragraph*.27}%
\contentsline {section}{\numberline {1.6}Contains of transaction request}{13}{section.1.6}%
\contentsline {paragraph}{}{13}{paragraph*.28}%
\contentsline {paragraph}{}{13}{paragraph*.29}%
\contentsline {paragraph}{}{14}{paragraph*.30}%
\contentsline {paragraph}{}{14}{paragraph*.31}%
\contentsline {chapter}{\numberline {2}Solution}{15}{chapter.2}%
\contentsline {section}{\numberline {2.1}Introduction}{15}{section.2.1}%
\contentsline {paragraph}{}{15}{paragraph*.32}%
\contentsline {section}{\numberline {2.2}Other cryptocurrencies and types of wallets}{16}{section.2.2}%
\contentsline {paragraph}{}{16}{paragraph*.33}%
\contentsline {paragraph}{}{16}{paragraph*.34}%
\contentsline {section}{\numberline {2.3}System architecture}{17}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Order of operation}{17}{subsection.2.3.1}%
\contentsline {paragraph}{}{17}{paragraph*.35}%
\contentsline {subsection}{\numberline {2.3.2}Message types}{18}{subsection.2.3.2}%
\contentsline {paragraph}{}{18}{paragraph*.36}%
\contentsline {paragraph}{}{18}{paragraph*.37}%
\contentsline {subsubsection}{Request Message}{18}{subsubsection*.38}%
\contentsline {paragraph}{}{18}{paragraph*.39}%
\contentsline {subsubsection}{Response Message}{18}{subsubsection*.40}%
\contentsline {paragraph}{}{18}{paragraph*.41}%
\contentsline {subsection}{\numberline {2.3.3}Key processing}{20}{subsection.2.3.3}%
\contentsline {subsubsection}{The way of private key hiding}{20}{subsubsection*.42}%
\contentsline {paragraph}{}{20}{paragraph*.43}%
\contentsline {subsubsection}{Private key processing on processor side}{21}{subsubsection*.44}%
\contentsline {paragraph}{}{21}{paragraph*.45}%
\contentsline {paragraph}{}{21}{paragraph*.46}%
\contentsline {subsubsection}{Key processing on exchange side}{21}{subsubsection*.47}%
\contentsline {paragraph}{}{21}{paragraph*.48}%
\contentsline {section}{\numberline {2.4}Implementation}{23}{section.2.4}%
\contentsline {paragraph}{}{23}{paragraph*.49}%
\contentsline {subsection}{\numberline {2.4.1}Noisy file processing}{23}{subsection.2.4.1}%
\contentsline {paragraph}{}{23}{paragraph*.50}%
\contentsline {paragraph}{}{24}{paragraph*.51}%
\contentsline {paragraph}{}{24}{paragraph*.52}%
\contentsline {paragraph}{}{25}{paragraph*.53}%
\contentsline {subsection}{\numberline {2.4.2}Communication}{25}{subsection.2.4.2}%
\contentsline {paragraph}{}{25}{paragraph*.54}%
\contentsline {paragraph}{}{28}{paragraph*.55}%
\contentsline {paragraph}{}{29}{paragraph*.56}%
\contentsline {paragraph}{}{30}{paragraph*.57}%
\contentsline {paragraph}{}{32}{paragraph*.58}%
\contentsline {subsection}{\numberline {2.4.3}Cryptocurrency wallet manager}{32}{subsection.2.4.3}%
\contentsline {paragraph}{}{32}{paragraph*.59}%
\contentsline {section}{\numberline {2.5}Availability, purpose and license}{34}{section.2.5}%
\contentsline {paragraph}{}{34}{paragraph*.60}%
\contentsline {paragraph}{}{34}{paragraph*.61}%
\contentsline {paragraph}{}{34}{paragraph*.62}%
\contentsline {paragraph}{}{34}{paragraph*.63}%
\contentsline {chapter}{\numberline {3}Environment preparation}{35}{chapter.3}%
\contentsline {section}{\numberline {3.1}Machine selection}{35}{section.3.1}%
\contentsline {paragraph}{}{35}{paragraph*.64}%
\contentsline {section}{\numberline {3.2}Electrum installation}{35}{section.3.2}%
\contentsline {paragraph}{}{35}{paragraph*.65}%
\contentsline {section}{\numberline {3.3}Electrum wallet preparation}{36}{section.3.3}%
\contentsline {paragraph}{}{36}{paragraph*.66}%
\contentsline {paragraph}{}{36}{paragraph*.67}%
\contentsline {paragraph}{}{36}{paragraph*.68}%
\contentsline {section}{\numberline {3.4}Password creation}{37}{section.3.4}%
\contentsline {paragraph}{}{37}{paragraph*.69}%
\contentsline {section}{\numberline {3.5}Storage of the password}{38}{section.3.5}%
\contentsline {paragraph}{}{38}{paragraph*.70}%
\contentsline {section}{\numberline {3.6}Creation of other keys}{38}{section.3.6}%
\contentsline {paragraph}{}{38}{paragraph*.71}%
\contentsline {paragraph}{}{38}{paragraph*.72}%
\contentsline {section}{\numberline {3.7}Solution usage}{39}{section.3.7}%
\contentsline {paragraph}{}{39}{paragraph*.73}%
\contentsline {paragraph}{}{39}{paragraph*.74}%
\contentsline {paragraph}{}{40}{paragraph*.75}%
\contentsline {chapter}{\numberline {4}Tests}{41}{chapter.4}%
\contentsline {paragraph}{}{41}{paragraph*.76}%
\contentsline {section}{\numberline {4.1}Unit tests}{41}{section.4.1}%
\contentsline {paragraph}{}{41}{paragraph*.77}%
\contentsline {section}{\numberline {4.2}Workflow test}{42}{section.4.2}%
\contentsline {paragraph}{}{42}{paragraph*.78}%
\contentsline {subsection}{\numberline {4.2.1}Bitcoin testnet}{42}{subsection.4.2.1}%
\contentsline {paragraph}{}{42}{paragraph*.79}%
\contentsline {paragraph}{}{43}{paragraph*.80}%
\contentsline {subsection}{\numberline {4.2.2}Speed}{43}{subsection.4.2.2}%
\contentsline {paragraph}{}{43}{paragraph*.81}%
\contentsline {section}{\numberline {4.3}Vulnerabilities}{44}{section.4.3}%
\contentsline {paragraph}{}{44}{paragraph*.82}%
\contentsline {paragraph}{}{44}{paragraph*.83}%
\contentsline {paragraph}{}{44}{paragraph*.84}%
\contentsline {paragraph}{}{45}{paragraph*.85}%
\contentsline {chapter}{\numberline {5}Conclusion}{46}{chapter.5}%
\contentsline {paragraph}{}{46}{paragraph*.86}%
